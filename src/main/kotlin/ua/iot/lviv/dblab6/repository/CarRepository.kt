package ua.iot.lviv.dblab6.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ua.iot.lviv.dblab6.model.Car

@Repository
interface CarRepository : JpaRepository<Car, Int>

