package ua.iot.lviv.dblab6.exception

class RecordNotFoundException(message: String?) : Exception(message) {
}
