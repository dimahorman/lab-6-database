package ua.iot.lviv.dblab6.model

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "person")
data class Person(
        @Id @Column(name = "name")
        val name: String,
        @Column(name = "age")
        val age: BigDecimal,
        @Column(name = "city")
        val city: String,
        @Column(name = "license_number")
        val licenseNumber: BigDecimal
) {
        constructor() : this("",
                BigDecimal(0),
                "",
                BigDecimal(0)) {

        }

}
