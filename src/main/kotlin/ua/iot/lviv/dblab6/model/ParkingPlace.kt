package ua.iot.lviv.dblab6.model

import javax.persistence.*

@Entity
@Table(name = "parking_place")
data class ParkingPlace(
        @Id @GeneratedValue(strategy= GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "parking_garage_id")
        val parkingGarageId: Int
) {
        constructor() : this(0, 0)


}

