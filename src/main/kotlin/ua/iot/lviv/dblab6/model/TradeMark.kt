package ua.iot.lviv.dblab6.model

import javax.persistence.*

@Entity
@Table(name = "trade_mark")
data class TradeMark(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "title")
        val title: String,
        @Column(name = "owner")
        val owner: String
) {
    constructor() : this(
            0,
            "",
            "")


}
