package ua.iot.lviv.dblab6.model

import java.sql.Time
import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "coupon")
data class Coupon(
        @Id @GeneratedValue(strategy= GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "car_number")
        val carNumber: Int,
        @Column(name = "start_time")
        val startTime: Time,
        @Column(name = "end_time")
        val endTime: Time,
        @Column(name = "parking_place_id")
        val parkingPlaceId: Int,
        @Column(name = "parking_garage_id")
        val parkingGarageId: Int,
        @Column(name = "price")
        val price: Double
)
{
        constructor() : this(0, 0,
                Time(0L),
                Time(0L),
                0,
                0,
                0.0
        )
}
