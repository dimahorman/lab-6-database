package ua.iot.lviv.dblab6.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "car")
data class Car(
        @Id @Column(name = "number")
        val number: Int,
        @Column(name = "model")
        val model: String,
        @Column(name = "trade_mark")
        val tradeMark: String,
        @Column(name = "driver_name")
        val driverName: String
) {
        constructor() : this(0, "", "", "")


}
