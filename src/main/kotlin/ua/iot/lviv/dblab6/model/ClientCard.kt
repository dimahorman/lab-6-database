package ua.iot.lviv.dblab6.model

import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "client_card")
data class ClientCard(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "start_time")
        val startTime: Timestamp,
        @Column(name = "end_time")
        val endTime: Timestamp,
        @Column(name = "person_name")
        val personName: String,
        @Column(name = "price")
        val price: Double,
        @Column(name = "parking_place_id")
        val parkingPlaceId: Int,
        @Column(name = "parking_garage_id")
        val parkingGarageId: Int
) {
    constructor() : this(0, Timestamp(0L),
            Timestamp(0L),
            "",
            0.0,
            0,
            0
    )


}
