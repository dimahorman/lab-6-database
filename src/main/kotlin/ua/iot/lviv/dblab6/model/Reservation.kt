package ua.iot.lviv.dblab6.model

import java.sql.Timestamp
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "reservation")
data class Reservation(
        @Id @GeneratedValue(strategy= GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "car_number")
        val carNumber: Int,
        @Column(name = "person_name")
        val personName: String,
        @Column(name = "start_time")
        val startTime: Timestamp,
        @Column(name = "end_time")
        val endTime: Timestamp,
        @Column(name = "parking_place_id")
        val parkingPlaceId: Int,
        @Column(name = "parking_garage_id")
        val parkingGarageId: Int,
        @Column(name = "price")
        val price: Double
) {
        constructor() : this(0,
                0,
                "",
                Timestamp(0),
                Timestamp(0),
                0,
                0,
                0.0)


}

