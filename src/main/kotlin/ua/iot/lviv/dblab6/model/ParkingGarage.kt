package ua.iot.lviv.dblab6.model

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "parking_garage")
data class ParkingGarage(
        @Id @GeneratedValue(strategy= GenerationType.IDENTITY) @Column(name = "id")
        val id: Int,
        @Column(name = "name")
        val name: String,
        @Column(name = "address")
        val address: String,
        @Column(name = "occupancy")
        val occupancy: BigDecimal,
        @Column(name = "place_quantity")
        val placeQuantity: BigDecimal,
        @Column(name = "trade_mark_id")
        val tradeMarkId: Int
) {
        constructor() : this(0,
                "",
                "",
                BigDecimal(0),
                BigDecimal(0),
                0)


}

