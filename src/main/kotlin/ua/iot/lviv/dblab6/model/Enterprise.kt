package ua.iot.lviv.dblab6.model

import javax.persistence.*

@Entity
@Table(name = "enterprise")
data class Enterprise(
        @Id  @Column(name = "title")
        val title: String
) {
        constructor() : this("") {

        }
}
