package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Enterprise
import ua.iot.lviv.dblab6.model.EnterpriseCard
import ua.iot.lviv.dblab6.service.EnterpriseCardService

@RestController
@RequestMapping("/enterprise-cards")
class EnterpriseCardController{
    @Autowired
    private val service = EnterpriseCardService()

    @GetMapping
    fun getAllEnterpriseCards(): ResponseEntity<List<EnterpriseCard>> {
        val list = service.getAllEnterpriseCards()

        return ResponseEntity<List<EnterpriseCard>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getEnterpriseCardById(@PathVariable("id") id: Int): ResponseEntity<EnterpriseCard> {
        val entity = service.getEnterpriseCardById(id)

        return ResponseEntity<EnterpriseCard>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateEnterpriseCard(enterpriseCard: EnterpriseCard): ResponseEntity<EnterpriseCard> {
        val updated = service.createOrUpdateEnterpriseCard(enterpriseCard)
        return ResponseEntity<EnterpriseCard>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateEnterpriseCard(enterpriseCard: EnterpriseCard): ResponseEntity<EnterpriseCard> {
        val updated = service.createOrUpdateEnterpriseCard(enterpriseCard)
        return ResponseEntity<EnterpriseCard>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteEnterpriseCardById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteEnterpriseCardById(id)
        return HttpStatus.FORBIDDEN
    }

}
