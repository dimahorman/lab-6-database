package ua.iot.lviv.dblab6.api

import org.springframework.http.HttpStatus
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.model.Car
import ua.iot.lviv.dblab6.service.CarService


@RestController
@RequestMapping("/cars")
class CarController {
    @Autowired
   private val service = CarService()

    @GetMapping
    fun getAllCars(): ResponseEntity<List<Car>> {
        val list = service.getAllCars()

        return ResponseEntity<List<Car>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getCarById(@PathVariable("id") id: Int): ResponseEntity<Car> {
        val entity = service.getCarById(id)

        return ResponseEntity<Car>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCar(car: Car): ResponseEntity<Car> {
        val updated = service.createOrUpdateCar(car)
        return ResponseEntity<Car>(updated, HttpHeaders(), HttpStatus.OK)
    }
    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateCar(car: Car): ResponseEntity<Car> {
        val updated = service.createOrUpdateCar(car)
        return ResponseEntity<Car>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteCarById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteCarById(id)
        return HttpStatus.FORBIDDEN
    }

}
