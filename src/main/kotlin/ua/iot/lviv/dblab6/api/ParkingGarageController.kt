package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.EnterpriseCard
import ua.iot.lviv.dblab6.model.ParkingGarage
import ua.iot.lviv.dblab6.service.ParkingGarageService

@RestController
@RequestMapping("/parking-garages")
class ParkingGarageController {
    @Autowired
    private val service = ParkingGarageService()

    @GetMapping
    fun getAllParkingGarages(): ResponseEntity<List<ParkingGarage>> {
        val list = service.getAllParkingGarages()

        return ResponseEntity<List<ParkingGarage>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getParkingGarageById(@PathVariable("id") id: Int): ResponseEntity<ParkingGarage> {
        val entity = service.getParkingGarageById(id)

        return ResponseEntity<ParkingGarage>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateParkingGarage(parkingGarage: ParkingGarage): ResponseEntity<ParkingGarage> {
        val updated = service.createOrUpdateParkingGarage(parkingGarage)
        return ResponseEntity<ParkingGarage>(updated, HttpHeaders(), HttpStatus.OK)
    }
    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateParkingGarage(parkingGarage: ParkingGarage): ResponseEntity<ParkingGarage> {
        val updated = service.createOrUpdateParkingGarage(parkingGarage)
        return ResponseEntity<ParkingGarage>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteParkingGarageById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteParkingGarageById(id)
        return HttpStatus.FORBIDDEN
    }
}
