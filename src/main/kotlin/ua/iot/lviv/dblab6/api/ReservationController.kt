package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Person
import ua.iot.lviv.dblab6.model.Reservation
import ua.iot.lviv.dblab6.service.ReservationService

@RestController
@RequestMapping("/reservations")
class ReservationController {
    @Autowired
    private val service = ReservationService()

    @GetMapping
    fun getAllReservations(): ResponseEntity<List<Reservation>> {
        val list = service.getAllReservations()

        return ResponseEntity<List<Reservation>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getReservationById(@PathVariable("id") id: Int): ResponseEntity<Reservation> {
        val entity = service.getReservationById(id)

        return ResponseEntity<Reservation>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateReservation(reservation: Reservation): ResponseEntity<Reservation> {
        val updated = service.createOrUpdateReservation(reservation)
        return ResponseEntity<Reservation>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateReservation(reservation: Reservation): ResponseEntity<Reservation> {
        val updated = service.createOrUpdateReservation(reservation)
        return ResponseEntity<Reservation>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteReservationById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteReservationById(id)
        return HttpStatus.FORBIDDEN
    }
}
