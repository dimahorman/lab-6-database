package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ParkingGarage
import ua.iot.lviv.dblab6.model.ParkingPlace
import ua.iot.lviv.dblab6.service.ParkingPlaceService

@RestController
@RequestMapping("/parking-places")
class ParkingPlaceController {
    @Autowired
    private val service = ParkingPlaceService()

    @GetMapping
    fun getAllParkingPlaces(): ResponseEntity<List<ParkingPlace>> {
        val list = service.getAllParkingPlaces()

        return ResponseEntity<List<ParkingPlace>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getParkingPlaceById(@PathVariable("id") id: Int): ResponseEntity<ParkingPlace> {
        val entity = service.getParkingPlaceById(id)

        return ResponseEntity<ParkingPlace>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateParkingPlace(parkingPlace: ParkingPlace): ResponseEntity<ParkingPlace> {
        val updated = service.createOrUpdateParkingPlace(parkingPlace)
        return ResponseEntity<ParkingPlace>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateParkingPlace(parkingPlace: ParkingPlace): ResponseEntity<ParkingPlace> {
        val updated = service.createOrUpdateParkingPlace(parkingPlace)
        return ResponseEntity<ParkingPlace>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteParkingPlaceById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteParkingPlaceById(id)
        return HttpStatus.FORBIDDEN
    }
}
