package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Reservation
import ua.iot.lviv.dblab6.model.TradeMark
import ua.iot.lviv.dblab6.service.TradeMarkService

@RestController
@RequestMapping("/trademarks")
class TradeMarkController {
    @Autowired
    private val service = TradeMarkService()

    @GetMapping
    fun getAllTradeMarks(): ResponseEntity<List<TradeMark>> {
        val list = service.getAllTradeMarks()

        return ResponseEntity<List<TradeMark>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getTradeMarkById(@PathVariable("id") id: Int): ResponseEntity<TradeMark> {
        val entity = service.getTradeMarkById(id)

        return ResponseEntity<TradeMark>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateTradeMark(tradeMark: TradeMark): ResponseEntity<TradeMark> {
        val updated = service.createOrUpdateTradeMark(tradeMark)
        return ResponseEntity<TradeMark>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateTradeMark(tradeMark: TradeMark): ResponseEntity<TradeMark> {
        val updated = service.createOrUpdateTradeMark(tradeMark)
        return ResponseEntity<TradeMark>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteTradeMarkById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteTradeMarkById(id)
        return HttpStatus.FORBIDDEN
    }
}
