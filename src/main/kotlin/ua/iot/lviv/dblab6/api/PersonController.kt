package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ParkingPlace
import ua.iot.lviv.dblab6.model.Person
import ua.iot.lviv.dblab6.service.PersonService

@RestController
@RequestMapping("/persons")
class PersonController {
    @Autowired
    private val service = PersonService()

    @GetMapping
    fun getAllPersons(): ResponseEntity<List<Person>> {
        val list = service.getAllPersons()

        return ResponseEntity<List<Person>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getPersonById(@PathVariable("id") id: String): ResponseEntity<Person> {
        val entity = service.getPersonById(id)

        return ResponseEntity<Person>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdatePerson(person: Person): ResponseEntity<Person> {
        val updated = service.createOrUpdatePerson(person)
        return ResponseEntity<Person>(updated, HttpHeaders(), HttpStatus.OK)
    }
    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updatePerson(person: Person): ResponseEntity<Person> {
        val updated = service.createOrUpdatePerson(person)
        return ResponseEntity<Person>(updated, HttpHeaders(), HttpStatus.OK)
    }


    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deletePersonById(@PathVariable("id") id: String): HttpStatus {
        service.deletePersonById(id)
        return HttpStatus.FORBIDDEN
    }
}
