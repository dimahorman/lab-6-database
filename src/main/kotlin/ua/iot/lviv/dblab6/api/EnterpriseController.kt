package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Coupon
import ua.iot.lviv.dblab6.model.Enterprise
import ua.iot.lviv.dblab6.service.CouponService
import ua.iot.lviv.dblab6.service.EnterpriseService

@RestController
@RequestMapping("/enterprises")
class EnterpriseController {
    @Autowired
    private val service = EnterpriseService()

    @GetMapping
    fun getAllEnterprises(): ResponseEntity<List<Enterprise>> {
        val list = service.getAllEnterprises()

        return ResponseEntity<List<Enterprise>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getEnterpriseById(@PathVariable("id") id: String): ResponseEntity<Enterprise> {
        val entity = service.getEnterpriseById(id)

        return ResponseEntity<Enterprise>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateEnterprise(enterprise: Enterprise): ResponseEntity<Enterprise> {
        val updated = service.createOrUpdateEnterprise(enterprise)
        return ResponseEntity<Enterprise>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateEnterprise(enterprise: Enterprise): ResponseEntity<Enterprise> {
        val updated = service.createOrUpdateEnterprise(enterprise)
        return ResponseEntity<Enterprise>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteEnterpriseById(@PathVariable("id") id: String): HttpStatus {
        service.deleteEnterpriseById(id)
        return HttpStatus.FORBIDDEN
    }
}
