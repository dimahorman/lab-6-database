package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ClientCard
import ua.iot.lviv.dblab6.model.Coupon
import ua.iot.lviv.dblab6.service.ClientCardService
import ua.iot.lviv.dblab6.service.CouponService

@RestController
@RequestMapping("/coupons")
class CouponController {
    @Autowired
    private val service = CouponService()

    @GetMapping
    fun getAllCoupons(): ResponseEntity<List<Coupon>> {
        val list = service.getAllCoupons()

        return ResponseEntity<List<Coupon>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getCouponById(@PathVariable("id") id: Int): ResponseEntity<Coupon> {
        val entity = service.getCouponById(id)

        return ResponseEntity<Coupon>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCoupon(coupon: Coupon): ResponseEntity<Coupon> {
        val updated = service.createOrUpdateCoupon(coupon)
        return ResponseEntity<Coupon>(updated, HttpHeaders(), HttpStatus.OK)
    }
    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateCoupon(coupon: Coupon): ResponseEntity<Coupon> {
        val updated = service.createOrUpdateCoupon(coupon)
        return ResponseEntity<Coupon>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteCouponById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteCouponById(id)
        return HttpStatus.FORBIDDEN
    }
}
