package ua.iot.lviv.dblab6.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Car
import ua.iot.lviv.dblab6.model.ClientCard
import ua.iot.lviv.dblab6.service.ClientCardService

@RestController
@RequestMapping("/client-cards")
class ClientCardController {
    @Autowired
    private val service = ClientCardService()

    @GetMapping
    fun getAllClientCards(): ResponseEntity<List<ClientCard>> {
        val list = service.getAllClientCards()

        return ResponseEntity<List<ClientCard>>(list, HttpHeaders(), HttpStatus.OK)
    }

    @GetMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun getClientCardById(@PathVariable("id") id: Int): ResponseEntity<ClientCard> {
        val entity = service.getClientCardById(id)

        return ResponseEntity<ClientCard>(entity, HttpHeaders(), HttpStatus.OK)
    }

    @PostMapping
    @Throws(RecordNotFoundException::class)
    fun createOrUpdateClientCard(clientCard: ClientCard): ResponseEntity<ClientCard> {
        val updated = service.createOrUpdateClientCard(clientCard)
        return ResponseEntity<ClientCard>(updated, HttpHeaders(), HttpStatus.OK)
    }
    @PutMapping
    @Throws(RecordNotFoundException::class)
    fun updateClientCard(clientCard: ClientCard): ResponseEntity<ClientCard> {
        val updated = service.createOrUpdateClientCard(clientCard)
        return ResponseEntity<ClientCard>(updated, HttpHeaders(), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    @Throws(RecordNotFoundException::class)
    fun deleteClientCardById(@PathVariable("id") id: Int): HttpStatus {
        service.deleteClientCardById(id)
        return HttpStatus.FORBIDDEN
    }
}
