package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ClientCard
import ua.iot.lviv.dblab6.model.Coupon
import ua.iot.lviv.dblab6.repository.CouponRepository
import java.util.ArrayList

@Service
class CouponService {

    @Autowired
    private lateinit var repository: CouponRepository

    fun getAllCoupons(): List<Coupon> {
        val couponList = repository.findAll()

        return if (couponList.size > 0) {
            couponList
        } else {
            ArrayList<Coupon>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getCouponById(id: Int): Coupon {
        val coupon = repository.findById(id)

        return if (coupon.isPresent) {
            coupon.get()
        } else {
            throw RecordNotFoundException("No coupon record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteCouponById(id: Int) {
        val car = repository.findById(id)

        if (car.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No coupon record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCoupon(entity: Coupon): Coupon {
        val coupon = repository.findById(entity.id)

        return if (coupon.isPresent) {
            var newEntity = Coupon(
                    entity.id,
                    entity.carNumber,
                    entity.startTime,
                    entity.endTime,
                    entity.parkingPlaceId,
                    entity.parkingGarageId,
                    entity.price
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
