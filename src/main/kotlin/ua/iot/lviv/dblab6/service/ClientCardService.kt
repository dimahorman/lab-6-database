package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ClientCard
import ua.iot.lviv.dblab6.repository.ClientCardRepository
import java.util.ArrayList

@Service
class ClientCardService {
    @Autowired
    private lateinit var repository: ClientCardRepository

    fun getAllClientCards(): List<ClientCard> {
        val clientCardList = repository.findAll()

        return if (clientCardList.size > 0) {
            clientCardList
        } else {
            ArrayList<ClientCard>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getClientCardById(id: Int): ClientCard {
        val clientCard = repository.findById(id)

        return if (clientCard.isPresent) {
            clientCard.get()
        } else {
            throw RecordNotFoundException("No clientCard record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteClientCardById(id: Int) {
        val car = repository.findById(id)

        if (car.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No employee record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateClientCard(entity: ClientCard): ClientCard {
        val clientCard = repository.findById(entity.id)

        return if (clientCard.isPresent) {
            var newEntity = ClientCard(
                    entity.id,
                    entity.startTime,
                    entity.endTime,
                    entity.personName,
                    entity.price,
                    entity.parkingPlaceId,
                    entity.parkingGarageId
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
