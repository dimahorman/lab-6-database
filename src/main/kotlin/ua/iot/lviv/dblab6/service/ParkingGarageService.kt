package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ParkingGarage
import ua.iot.lviv.dblab6.model.ParkingPlace
import ua.iot.lviv.dblab6.repository.ParkingGarageRepository
import java.util.ArrayList

@Service
class ParkingGarageService {
    @Autowired
    private lateinit var repository: ParkingGarageRepository

    fun getAllParkingGarages(): List<ParkingGarage> {
        val parkingGarageList = repository.findAll()

        return if (parkingGarageList.size > 0) {
            parkingGarageList
        } else {
            ArrayList<ParkingGarage>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getParkingGarageById(id: Int): ParkingGarage {
        val parkingGarage = repository.findById(id)

        return if (parkingGarage.isPresent) {
            parkingGarage.get()
        } else {
            throw RecordNotFoundException("No ParkingGarage record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteParkingGarageById(id: Int) {
        val parkingGarage = repository.findById(id)

        if (parkingGarage.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No ParkingGarage record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateParkingGarage(entity: ParkingGarage): ParkingGarage {
        val parkingGarage = repository.findById(entity.id)

        return if (parkingGarage.isPresent) {
            var newEntity = ParkingGarage(
                    entity.id,
                    entity.name,
                    entity.address,
                    entity.occupancy,
                    entity.placeQuantity,
                    entity.tradeMarkId
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
