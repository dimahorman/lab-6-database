package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Enterprise
import ua.iot.lviv.dblab6.repository.EnterpriseRepository
import java.util.ArrayList

@Service
class EnterpriseService {
    @Autowired
    private lateinit var repository: EnterpriseRepository

    fun getAllEnterprises(): List<Enterprise> {
        val enterpriseList = repository.findAll()

        return if (enterpriseList.size > 0) {
            enterpriseList
        } else {
            ArrayList<Enterprise>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getEnterpriseById(title: String): Enterprise {
        val enterprise = repository.findById(title)

        return if (enterprise.isPresent) {
            enterprise.get()
        } else {
            throw RecordNotFoundException("No enterprise record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteEnterpriseById(title: String) {
        val enterprise = repository.findById(title)

        if (enterprise.isPresent) {
            repository.deleteById(title)
        } else {
            throw RecordNotFoundException("No coupon record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateEnterprise(entity: Enterprise): Enterprise {
        val enterprise = repository.findById(entity.title)

        return if (enterprise.isPresent) {
            var newEntity = Enterprise(
                    entity.title

            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }

}
