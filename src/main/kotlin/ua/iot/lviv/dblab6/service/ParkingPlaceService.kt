package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.ParkingPlace
import ua.iot.lviv.dblab6.repository.ParkingPlaceRepository
import java.util.ArrayList

@Service
class ParkingPlaceService {
    @Autowired
    private lateinit var repository: ParkingPlaceRepository

    fun getAllParkingPlaces(): List<ParkingPlace> {
        val parkingPlaceList = repository.findAll()

        return if (parkingPlaceList.size > 0) {
            parkingPlaceList
        } else {
            ArrayList<ParkingPlace>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getParkingPlaceById(id: Int): ParkingPlace {
        val parkingPlace = repository.findById(id)

        return if (parkingPlace.isPresent) {
            parkingPlace.get()
        } else {
            throw RecordNotFoundException("No ParkingPlace record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteParkingPlaceById(id: Int) {
        val parkingPlace = repository.findById(id)

        if (parkingPlace.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No ParkingPlace record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateParkingPlace(entity: ParkingPlace): ParkingPlace {
        val parkingPlace = repository.findById(entity.id)

        return if (parkingPlace.isPresent) {
            var newEntity = ParkingPlace(
                   entity.id,
                    entity.parkingGarageId
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
