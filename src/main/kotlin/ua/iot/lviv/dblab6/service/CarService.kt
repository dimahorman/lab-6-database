package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.model.Car
import ua.iot.lviv.dblab6.repository.CarRepository
import java.util.ArrayList
import ua.iot.lviv.dblab6.exception.RecordNotFoundException


@Service
class CarService {
    @Autowired
    private lateinit var repository: CarRepository

    fun getAllCars(): List<Car> {
        val carList = repository.findAll()

        return if (carList.size > 0) {
            carList
        } else {
            ArrayList<Car>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getCarById(id: Int): Car {
        val car = repository.findById(id)

        return if (car.isPresent) {
            car.get()
        } else {
            throw RecordNotFoundException("No employee record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteCarById(id: Int) {
        val car = repository.findById(id)

        if (car.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No employee record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCar(entity: Car): Car {
        val car = repository.findById(entity.number)

        return if (car.isPresent) {
            var newEntity = Car(entity.number,
                    entity.model,
                    entity.tradeMark,
                    entity.driverName)

            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
