package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Reservation
import ua.iot.lviv.dblab6.repository.ReservationRepository
import java.util.ArrayList

@Service
class ReservationService {
    @Autowired
    private lateinit var repository: ReservationRepository

    fun getAllReservations(): List<Reservation> {
        val reservationList = repository.findAll()

        return if (reservationList.size > 0) {
            reservationList
        } else {
            ArrayList<Reservation>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getReservationById(id: Int): Reservation {
        val reservation = repository.findById(id)

        return if (reservation.isPresent) {
            reservation.get()
        } else {
            throw RecordNotFoundException("No Person record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteReservationById(id: Int) {
        val reservation = repository.findById(id)

        if (reservation.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No Person record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateReservation(entity: Reservation): Reservation {
        val reservation = repository.findById(entity.id)

        return if (reservation.isPresent) {
            var newEntity = Reservation(
                    entity.id,
                    entity.carNumber,
                    entity.personName,
                    entity.startTime,
                    entity.endTime,
                    entity.parkingPlaceId,
                    entity.parkingGarageId,
                    entity.price
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
