package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.EnterpriseCard
import ua.iot.lviv.dblab6.repository.EnterpriseCardRepository
import java.util.ArrayList

@Service
class EnterpriseCardService {
    @Autowired
    private lateinit var repository: EnterpriseCardRepository

    fun getAllEnterpriseCards(): List<EnterpriseCard> {
        val enterpriseCardList = repository.findAll()

        return if (enterpriseCardList.size > 0) {
            enterpriseCardList
        } else {
            ArrayList<EnterpriseCard>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getEnterpriseCardById(id: Int): EnterpriseCard {
        val enterpriseCard = repository.findById(id)

        return if (enterpriseCard.isPresent) {
            enterpriseCard.get()
        } else {
            throw RecordNotFoundException("No enterprise card record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteEnterpriseCardById(id: Int) {
        val enterprise = repository.findById(id)

        if (enterprise.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No enterprise card record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateEnterpriseCard(entity: EnterpriseCard): EnterpriseCard {
        val enterpriseCard = repository.findById(entity.id)

        return if (enterpriseCard.isPresent) {
            var newEntity = EnterpriseCard(
                    entity.parkingPlaceId,
                    entity.parkingGarageId,
                    entity.enterpriseTitle,
                    entity.id,
                    entity.startTime,
                    entity.endTime,
                    entity.price
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
