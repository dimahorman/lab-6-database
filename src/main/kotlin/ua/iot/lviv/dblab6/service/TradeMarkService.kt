package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.TradeMark
import ua.iot.lviv.dblab6.repository.TradeMarkRepository
import java.util.ArrayList

@Service
class TradeMarkService {
    @Autowired
    private lateinit var repository: TradeMarkRepository

    fun getAllTradeMarks(): List<TradeMark> {
        val tradeMarkList = repository.findAll()

        return if (tradeMarkList.size > 0) {
            tradeMarkList
        } else {
            ArrayList<TradeMark>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getTradeMarkById(id: Int): TradeMark {
        val tradeMark = repository.findById(id)

        return if (tradeMark.isPresent) {
            tradeMark.get()
        } else {
            throw RecordNotFoundException("No TradeMark record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deleteTradeMarkById(id: Int) {
        val tradeMark = repository.findById(id)

        if (tradeMark.isPresent) {
            repository.deleteById(id)
        } else {
            throw RecordNotFoundException("No TradeMark record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateTradeMark(entity: TradeMark): TradeMark {
        val tradeMark = repository.findById(entity.id)

        return if (tradeMark.isPresent) {
            var newEntity = TradeMark(
                    entity.id,
                    entity.title,
                    entity.owner
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
