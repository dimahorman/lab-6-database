package ua.iot.lviv.dblab6.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ua.iot.lviv.dblab6.exception.RecordNotFoundException
import ua.iot.lviv.dblab6.model.Person
import ua.iot.lviv.dblab6.model.TradeMark
import ua.iot.lviv.dblab6.repository.PersonRepository
import java.util.ArrayList

@Service
class PersonService {
    @Autowired
    private lateinit var repository: PersonRepository

    fun getAllPersons(): List<Person> {
        val personList = repository.findAll()

        return if (personList.size > 0) {
            personList
        } else {
            ArrayList<Person>()
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getPersonById(name: String): Person {
        val person = repository.findById(name)

        return if (person.isPresent) {
            person.get()
        } else {
            throw RecordNotFoundException("No Person record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun deletePersonById(name: String) {
        val tradeMark = repository.findById(name)

        if (tradeMark.isPresent) {
            repository.deleteById(name)
        } else {
            throw RecordNotFoundException("No Person record exist for given id")
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdatePerson(entity: Person): Person {
        val person = repository.findById(entity.name)

        return if (person.isPresent) {
            var newEntity = Person(
                    entity.name,
                    entity.age,
                    entity.city,
                    entity.licenseNumber
            )
            newEntity = repository.save(newEntity)

            newEntity
        } else {
            val entity1 = repository.save(entity)

            entity1
        }
    }
}
