package ua.iot.lviv.dblab6

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DbLab6Application

fun main(args: Array<String>) {
    runApplication<DbLab6Application>(*args)
}
